provider "aws" {
    region = "us-east-1"

}
# Creating VPC
module "vpc" {
    source       = "../vpc"
    cidr_block   = "10.0.0.0/16"
    vpc_Name_tag = "my_vpc"
}
#Creating Internet Gateway
module "inter_gateway" {
    source       = "../inter_gateway" 
    vpc_id       = "${module.vpc.id}" 
    ig_tag_Name  = "igw"
}
#Creating First Public Subnets
module "pub1_subnet" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.1.0/24"
  availabe_zone  = "us-east-1a"
  tag_name_value = "pub1_subnet"
}
#Creating Second Public Subnets
module "pub2_subnet" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.2.0/24"
  availabe_zone  = "us-east-1b"
  tag_name_value = "pub2_subnet"
}
#Creating First Private Subnet for Management
module "pri1_subnet_management" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.3.0/24"
  availabe_zone  = "us-east-1a"
  tag_name_value = "pri1_subnet"
}
#Creating Second Private Subnet for Mangement
module "pri2_subnet_management" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.4.0/24"
  availabe_zone  = "us-east-1b"
  tag_name_value = "pri2_subnet"
}
#Creating First Private Subnet for App Servers
module "pri1_subnet_appserver" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.5.0/24"
  availabe_zone  = "us-east-1a"
  tag_name_value = "pri1_subnet_appserver"
}
#Creating Second Private Subnet for App Servers
module "pri2_subnet_appserver" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.6.0/24"
  availabe_zone  = "us-east-1b"
  tag_name_value = "pri2_subnet_appserver"
}
#Creating First Private Subnet for DB
module "pri1_subnet_db" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.7.0/24"
  availabe_zone  = "us-east-1a"
  tag_name_value = "pri1_subnet_db"
}
#Creating Second Private Subnet for db
module "pri2_subnet_db" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.8.0/24"
  availabe_zone  = "us-east-1b"
  tag_name_value = "pri2_subnet_db"
}

#Creating First Private Subnet for monitoring
module "pri1_subnet_monitoring" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.9.0/24"
  availabe_zone  = "us-east-1a"
  tag_name_value = "pri1_subnet_monitoring"
}
#Creating Second Private Subnet for monitoring
module "pri2_subnet_monitoring" {
  source         = "../subnets"
  vpc_id         = "${module.vpc.id}"
  cidr_subnet    = "10.0.10.0/24"
  availabe_zone  = "us-east-1b"
  tag_name_value = "pri2_subnet_monitoring"
}
#Creating Nat Gateway
module "nat_gateway" {
  source         = "../nat"
  subnet_id      = "${module.pub1_subnet.id}"
  tag_Name_value = "terra_nat_gateway"
}
#Creating route table for Public Subnets
module "pub_routetable" {
  source                = "../routetable"
  vpc_id                = "${module.vpc.id}"
  cidr_block_routetable = "0.0.0.0/0"
  gateway_id            = "${module.inter_gateway.inter_gateway_id}"
  tag_Name_value        = "terra_pub_subnet"
}
# Associate Route Table with public subnets
module "pub1_association_to_routetable" {
  source                = "../associations_routetable"
  subnet_id             = "${module.pub1_subnet.id}"
  routetable_id         = "${module.pub_routetable.routetable_id}"
}
module "pub2_association_to_routetable" {
  source                = "../associations_routetable"
  subnet_id             = "${module.pub2_subnet.id}"
  routetable_id         = "${module.pub_routetable.routetable_id}"
}
# Creating Route Table for Private Management subnets
module "terra_pri_routetable" {
  source                = "../routetable"
  vpc_id                = "${module.vpc.id}"
  cidr_block_routetable = "0.0.0.0/0"
  gateway_id            = "${module.nat_gateway.nat_gateway_id}"
  tag_Name_value        = "terra_management_subnet"
}
# Associate Route Table with Private management subnets
module "pri1_manage_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri1_subnet_management.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
module "pri2_manage_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri2_subnet_management.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
# Associate Route Table with Private App Subnets
module "pri1_app_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri1_subnet_appserver.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
module "pri2_app_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri2_subnet_appserver.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
# Creating Route Tables with Private DB Subnets
module "pri1_db_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri1_subnet_db.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
module "pri2_db_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri2_subnet_db.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}

# Creating Route Tables with Private monitoring Subnets
module "pri1_monitoring_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri1_subnet_monitoring.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
module "pri2_monitoring_association_routetable" {
  source         = "../associations_routetable"
  subnet_id      = "${module.pri2_subnet_monitoring.id}"
  routetable_id  = "${module.terra_pri_routetable.routetable_id}"
}
# Creating Security group for intstances
module "security_group" {
  source           = "../securitygroup"
  sg_name          = "terra_sg"
  vpc_id           = "${module.vpc.id}"
  from_port_ssh    = "22"
  to_port_ssh      = "22"
  protocol_ssh     = "tcp"
  from_port_http   = "8080"
  to_port_http     = "8080"
  protocol_http    = "tcp"
  cidr_block       = ["0.0.0.0/0"]
  tag_name_value   = "terra_sg"
}
# Adding key
module "key_pair" {
  source = "../keypair"
  key_name = "access_key"
  key_path = "~/.ssh/id_rsa.pub"   
}
# #Launching public instance
module "pub_instance" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-0c2471b1a0c9aa67c"
  #availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pub2_subnet.id}"
  associate_public_ip = "true"
  tag_name_value      = "terra_pub_instance"
  tag_own_value       = "shweta"
}
#Lauching Private instance for manage jenkins and ansible
module "pri_jenkins" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-051cdae31bce439ce"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_management.id}"
  associate_public_ip = "false"
  tag_name_value      = "jenkins"  
  tag_own_value       = "shweta"
}

#Launching Private instance for app server
module "pri1_wp" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_appserver.id}"
  associate_public_ip = "false"
  tag_name_value      = "wordpress1"
  tag_own_value       = "shweta"
}
#Launching second private instance for tomcat server
module "pri2_wp" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1b"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri2_subnet_appserver.id}"
  associate_public_ip = "false"
  tag_name_value      = "wordpress2"
  tag_own_value       = "shweta"
}
#Launching private instance for db
module "pri1_db" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_db.id}"
  associate_public_ip = "false"
  tag_name_value      = "mysql" 
  tag_own_value       = "shweta" 
}
#Launching private instance for redis
module "pri1_redis_master" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_db.id}"
  associate_public_ip = "false"
  tag_name_value      = "redis_master"  
  tag_own_value       = "shweta"
}

#Launching private instance for redis slave
module "pri2_redis_slave1" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_db.id}"
  associate_public_ip = "false"
  tag_name_value      = "redis_slave1"  
  tag_own_value       = "shweta"
}

#Launching private instance for redis slave
module "pri2_redis_slave2" {
  source              = "../instances"
  instance_type       = "t2.nano"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_db.id}"
  associate_public_ip = "false"
  tag_name_value      = "redis_slave2"  
  tag_own_value       = "shweta"
}


#Launching private instance for monitioring
module "pri1_monitoring" {
  source              = "../instances"
  instance_type       = "t2.micro"
  ami_id              = "ami-03d3121d0708279b8"
  availability_zone   = "us-east-1a"
  key_name            = "${module.key_pair.key_id}"
  vpc_security_group  = ["${module.security_group.sg_id}"]
  subnet_id           = "${module.pri1_subnet_monitoring.id}"
  associate_public_ip = "false"
  tag_name_value      = "monitoring"  
  tag_own_value       = "shweta"
}



module "launch_config" {
    source = "../launch_conf"
    ami = "ami-03d3121d0708279b8"
    sg = "${module.security_group.sg_id}"
    keypair = "access_key"
}

module "autoscaling" {
    source = "../autoscaling_gr"
    launch_conf = "${module.launch_config.launch_id}"
    sub-1 = "${module.pub1_subnet.id}"
    sub-2 = "${module.pub2_subnet.id}"
}


module "alb" {
    source = "../alb"
    vpc_id = "${module.vpc.id}"
    alb_subnets = ["${module.pub1_subnet.id}","${module.pub2_subnet.id}"]
security_group_id = "${module.security_group.sg_id}"
jenkins_ec2_id = "${module.pri_jenkins.id}"
nginx_ec2_id = "${module.pri1_wp.id}"
grafana_ec2_id = "${module.pri1_monitoring.id}"
grafana1_ec2_id = "${module.pri1_monitoring.id}"
}
