resource "aws_instance" "terra_instance" {
    ami = "${var.ami_id}"
    instance_type = "${var.instance_type}"
    #availability_zone = "${var.availability_zone}"
    key_name = "${var.key_name}"
    vpc_security_group_ids  = ["${var.vpc_security_group}"]
    subnet_id = "${var.subnet_id}"
    associate_public_ip_address = "${var.associate_public_ip}"
    
    tags {
        Name = "${var.tag_name_value}"
        Owner = "${var.tag_own_value}"
    }
}