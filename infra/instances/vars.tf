variable "ami_id" {
    description = "ami id to lanuch instance"
}
variable "instance_type" {
    description = "The type of instances"
    default = "t2.micro"
}
variable "availability_zone" {
    description = "The availability zone to launch instance"
    default = "us-east-1a"
}
variable "key_name" {
    description = "the name of the key"
}
variable "vpc_security_group" {
    description = "security group to add with instance"
    type = "list"
}
variable "subnet_id" {
    description = "The vpc subnet id in which you want to lanuch instance"
}
variable "associate_public_ip" {
    description = "Do you want to associate public ip with instance, enter true if you want to"
    default = "true"
}
variable "tag_name_value" {
    description = "The value of tag Name"
    default = "terra_pub"
}
variable "tag_own_value" {
    description = "The value of tag Name"
    default = "terra_pub"
}





