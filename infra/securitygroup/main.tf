resource "aws_security_group" "terra_sg" {
    name = "${var.sg_name}"
    vpc_id = "${var.vpc_id}"
    
    ingress {
        from_port = "${var.from_port_ssh}"
        to_port = "${var.to_port_ssh}"
        protocol = "${var.protocol_ssh}"
        cidr_blocks = ["${var.cidr_block}"]
    }

    ingress {
        from_port = "${var.from_port_http}"
        to_port = "${var.to_port_http}"
        protocol = "${var.protocol_http}"
        cidr_blocks = ["${var.cidr_block}"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
      ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 6379
        to_port = 6379
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

      
     ingress {
        from_port = 3000
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

     ingress {
        from_port = 9090
        to_port = 9090
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
     
     ingress {
        from_port = 9100
        to_port = 9100
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 9093
        to_port = 9093
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 9121
        to_port = 9121
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
}


    tags {
        Name = "${var.tag_name_value}"
    }
}
